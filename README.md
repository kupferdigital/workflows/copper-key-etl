## Copper-key

![copper-key-etl-workflow](https://gitlab.com/kupferdigital/workflows/copper-key-etl/-/raw/main/workflow.svg)
```bash
# Install nextflow
curl -s https://get.nextflow.io | bash
# Insatll docker
sudo apt install podman
# Clone the repository
git clone https://gitlab.com/kupferdigital/workflows/copper-key-etl
# Run the workflow
cd copper-key-etl
make run
```
