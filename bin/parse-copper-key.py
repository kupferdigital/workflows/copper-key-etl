#!/usr/bin/env python

import concurrent.futures
import csv
import sys
from abc import ABC, abstractmethod

import requests
import urllib3
from bs4 import BeautifulSoup
from requests.adapters import HTTPAdapter
from urllib3 import Retry


class ContentPageParser(ABC):

    def __init__(self, id, content_page):
        self.id = id
        self.soup = BeautifulSoup(content_page, 'html.parser')

    @staticmethod
    def string(node): return str(node.string).strip()

    @staticmethod
    def strip(list): return map(lambda item: item.strip()
                                if isinstance(item, str) else item, list)

    @abstractmethod
    def get_header(self):
        pass

    @abstractmethod
    def get_rows(self):
        pass


class DatasheetParser(ContentPageParser):

    def __init__(self, id, content_page):
        super().__init__(id, content_page)

    def get_header(self):
        return ['id', 'datasheet']

    def get_rows(self):
        links = self.soup.find_all('a')
        datasheet = None
        if links:
            datasheet = links[0].get('href')
        print(self.id, datasheet)
        return [[self.id, datasheet]]


class MaterialParser(ContentPageParser):

    def __init__(self, id, content_page):
        super().__init__(id, content_page)

    def get_header(self):
        return ['id', 'symbol', 'number', 'norm', 'materialtype', 'isHeader']

    def get_material_row(self):
        material_type = self.soup.find(
            string='Type of Material:').find_next().text

        def none_if_not_found(search_string):
            tag = self.soup.find(string=search_string)
            if tag is not None:
                return tag.find_next().text

        symbol = none_if_not_found('Symbol:')
        number = none_if_not_found('Number')
        norm = none_if_not_found('Standard')
        return self.strip([self.id, symbol, number, norm, material_type, 1])

    def get_rows(self):
        rows = self.get_submaterial_rows()
        rows.append(self.get_material_row())
        return rows

    def get_submaterial_rows(self):
        rows = []
        submaterials = (
            self.soup
            .find(string='Standards')
            .parent
            .parent
            .next_sibling
            .next_siblings
        )
        for submaterial in submaterials:
            if submaterial.text.strip() == 'Remark':
                break
            material_type = submaterial.find(class_='kleins').text
            no_sy_nu = [
                s for s in submaterial.find(class_='dspalte1').strings]
            norm = no_sy_nu[0] if len(no_sy_nu) > 0 else ''
            symbol = no_sy_nu[1] if len(no_sy_nu) > 1 else ''
            number = no_sy_nu[2] if len(no_sy_nu) > 2 else ''
            row = self.strip([self.id, symbol, number, norm, material_type, 0])
            rows.append(row)
        return rows


class ChemicalCompositionParser(ContentPageParser):

    def __init__(self, id, content_page):
        super().__init__(id, content_page)
        self.element_nodes = self.get_element_nodes(self.soup)

    @staticmethod
    def get_element_nodes(soup):
        element_nodes = (soup
                         .find(string='Analysis:\xa0')
                         .findParent('td')
                         .findAll('b')
                         )
        return [
            node for node in element_nodes
            if 'Other one:' not in ContentPageParser.string(node)
        ]

    def get_header(self):
        header = ['id']
        header = header + [self.string(element_node) + postfix
                           for element_node in self.element_nodes
                           for postfix in ['_min', '_max']]
        return header

    def get_rows(self):
        row = [self.id]
        for element_node in self.element_nodes:
            min_node = element_node.findNext('td')
            max_node = min_node.findNext('td')
            row.append(self.string(min_node))
            row.append(self.string(max_node))
        return [row]


def get_ids():
    soup = BeautifulSoup(
        get_result_page(), 'html.parser')
    id_nodes = soup.find('select').find_all('option')
    id_to_country = {}
    for id_node in id_nodes:
        id = id_node['value']
        c = id_node.string
        country = c[c.find("[")+1:c.find("]")]
        id_to_country[id] = country
    return id_to_country


def get_result_page():
    return session.post(
        COPPER_KEY_URL + RESULTS_PATH,
        data={'mod': 'vt'},
        params=COPPER_KEY_PARAMS
    ).text


def get_content_pages(ids):
    id_to_content_page = {}
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future_to_id = {executor.submit(
            get_content_page, id): id
            for id in ids}
        for future in concurrent.futures.as_completed(future_to_id):
            id = future_to_id[future]
            try:
                content_page = future.result()
            except Exception as exception:
                print('%r generated an exception: %s' %
                      (id, exception))
            else:
                id_to_content_page[id] = content_page
    return id_to_content_page


def get_content_page(id):
    return session.post(
        COPPER_KEY_URL + CONTENT_PATH,
        data={'werkstoff': id},
        params=COPPER_KEY_PARAMS
    ).text


def write_content_pages(id_to_content_page):
    write_content_pages_to_csv(id_to_content_page,
                               CHEMICAL_COMPOSITIONS_FILE,
                               ChemicalCompositionParser)
    write_content_pages_to_csv(id_to_content_page,
                               MATERIALS_FILE,
                               MaterialParser)
    write_content_pages_to_csv(id_to_content_page,
                               DATASHEET_FILE,
                               DatasheetParser)


def write_content_pages_to_csv(id_to_content_page, file_name, parser_class):
    with open(file_name, 'w', newline='') as file:
        writer = csv.writer(file)
        header = True
        for id, content_page in id_to_content_page.items():
            parser = parser_class(id, content_page)
            if header:
                writer.writerow(parser.get_header())
                header = False
            writer.writerows(parser.get_rows())


def write_countries_csv(id_to_country):
    with open(COUNTRIES_FILE, 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['id', 'country'])
        for item in id_to_country.items():
            writer.writerow(item)


if __name__ == '__main__':
    COPPER_KEY_URL = 'https://standalone.kupferschluessel.de/'
    COPPER_KEY_PARAMS = {'lang': 'english'}
    RESULTS_PATH = 'ergebnisse.php'
    CONTENT_PATH = 'content.php'
    CHEMICAL_COMPOSITIONS_FILE = 'chemical-compositions.csv'
    MATERIALS_FILE = 'materials.csv'
    COUNTRIES_FILE = 'countries.csv'
    DATASHEET_FILE = 'datasheets.csv'

    session = requests.Session()
    adapter = HTTPAdapter(max_retries=Retry(total=4, backoff_factor=1,
                          allowed_methods=['POST'], status_forcelist=[429, 500, 502, 503, 504]))
    session.mount("http://", adapter)
    session.mount("https://", adapter)

    id_to_country = get_ids()
    if sys.argv[1] == 'true':
        id_to_country = {id: c for id, c in list(id_to_country.items())[::200]}
    write_countries_csv(id_to_country)
    id_to_content_page = get_content_pages(id_to_country.keys())
    write_content_pages(id_to_content_page)
