#!/usr/bin/env nextflow

params.serialization = 'nquads'

process RMLMAPPER {
    container 'localhost/rmlmapper:latest'

    input:
        path inputs
        path mappings

    output:
        path output, emit: graph

    script:
        file_extensions = [
            'nquads':'nq',
            'turtle':'ttl',
            'trig':'trig',
            'trix':'xml',
            'jsonld':'jsonld',
            'hdt':'hdt'
        ]
        output = 'graph.' + file_extensions[params.serialization]

    """
    java -jar /rmlmapper.jar \\
        -o ./$output \\
        --serialization $params.serialization \\
        --mappingfile $mappings
    """
}
