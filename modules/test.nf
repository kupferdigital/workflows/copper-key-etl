#!/usr/bin/env nextflow

include { RMLMAPPER } from './modules/rmlmapper/main'

workflow test_rmlmapper {
    mappings = channel.fromPath(['./tests/rmlmapper/mapping.ttl']).collect()
    inputs = channel.fromPath(['./tests/rmlmapper/sparseInput.csv']).collect()
    output = channel.fromPath(['./tests/rmlmapper/sparseInput.csv']).collect()
    RMLMAPPER(inputs, mappings)
    RMLMAPPER.out.graph.view()
}

workflow {
    test_rmlmapper()
}
