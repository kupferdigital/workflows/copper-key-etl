run:
	nextflow run main.nf -params-file params-prod.json -c secrets-prod.config

develop: build-image
	rm -f flowchart.svg
	nextflow run main.nf \
		-profile develop \
		-params-file params-dev.json \
		-c secrets-dev.config \
		# -with-dag flowchart.svg
	

build-image:
	podman build --tag copper-key-etl:local .


