process ParseCopperKey {
    output:
        path '*.csv'

    """
    parse-copper-key.py $params.sampleCopperKey
    process-copper-key.py
    """
}

include { CkanCreateResource } from params.modulesBasePath + 'ckan/main' addParams(
    url: params.ckan.url,
    apiToken: params.ckan.apiToken
)
include { ConvertDiagramToOntology } from params.modulesBasePath + 'chowlk/main'
include { FusekiUploadDataset } from params.modulesBasePath + 'fuseki/main' addParams(
    url: params.fuseki.url,
    user: 'admin',
    password: params.fuseki.password
)
include { MapToRdf } from params.modulesBasePath + 'mapper/main'
include { MergeRdf } from params.modulesBasePath + 'riot/main' addParams(
    serialization: 'nquads',
    formatted: false
)
include { RobotReason } from params.modulesBasePath + 'robot-reason/main'
include { NextflowPublish } from params.modulesBasePath + 'nextflow/main' addParams(
    outputDir: './output'
)


datasetName = 'copper-key'
ckanDbName = 'dki'
mapping = './input/copperkey-mapping.yml'

workflow PrepareAbox {
    main:
        copperKey = ParseCopperKey()
        MapToRdf(
            Channel.fromPath(mapping),
            copperKey
        )
        CkanCreateResource(copperKey.flatten(), ckanDbName, datasetName)
    emit:
        aBox = MapToRdf.out
}

workflow BuildGraph {
    take: aBox
    take: ontologyDiagram
    take: fusekiDatasetName

    main:
        tBox = ConvertDiagramToOntology(ontologyDiagram)
        graph = MergeRdf(aBox.mix(tBox).collect())
        reasonedGraph = RobotReason(graph)
        FusekiUploadDataset(reasonedGraph, fusekiDatasetName)
    emit:
        graph = reasonedGraph
}

workflow CopperKey {
    take: aBox
    main:
        BuildGraph(
            aBox,
            Channel.fromPath(params.ontology.copperKey),
            datasetName
        )
}

workflow CopperKeyIof {
    take: aBox
    main:
        BuildGraph(
            aBox,
            Channel.fromPath(params.ontology.copperKeyIof),
            datasetName + '-iof'
        )
        CkanCreateResource(BuildGraph.out.graph, ckanDbName, datasetName)
        NextflowPublish(BuildGraph.out.graph)
}

workflow {
    abox = PrepareAbox()
    CopperKey(abox)
    CopperKeyIof(abox)
}
